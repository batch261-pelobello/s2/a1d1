package com.zuitt.example;

import java.util.Scanner;

public class Activity1 {

    public static void main(String[] args){
        Scanner obj = new Scanner(System.in);
        System.out.println("Input year to be check if a leap year");
        int leapyear = obj.nextInt();
        if(leapyear%100 == 0){
            System.out.println(leapyear + " is not a leap year");
        } else if(leapyear%4 == 0 || leapyear%400 == 0){
            System.out.println(leapyear + " is a leap year");
        }else{
            System.out.println(leapyear + " is not a leap year");
        }



    }
}
