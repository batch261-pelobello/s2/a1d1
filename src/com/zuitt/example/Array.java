package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    //Java Collection
        // are a single unit of objects
        // useful for manipulation relevant pieces of data that can be used in different situation
    public static void main(String[] args){
            //Array are containers of values fo the Same data type given a pre-defined amount of values
            //Java arrays are more rigid, once the size and data type are defined they can no longer be change

            //Syntax: Array Declaration
                //dataType[] identifier = new dataType[numOfElements]
                //the value of the array is initialize to 0 or null

        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 98;


        //specific index in the array
        System.out.println(intArray[2]);

        //It prints the memory addres of the array
        System.out.println(intArray);

        //To print the intArray
        System.out.println(Arrays.toString(intArray));//print all data in the array


        boolean[] bArr = new boolean[2];
        System.out.println(Arrays.toString(bArr));

        //Declaring Array with Initialization
        //Syntax:
            //dataType[] identifier = {elementA, elementB, .....elementNth};
        String[] names= {"John","Jane","Joe"};
        System.out.println(Arrays.toString(names));

        //Check the length of the array
        System.out.println(names.length);

        //Sample Java Array methods
        Arrays.sort(intArray);
        System.out.println("Order of items after sort: "+ Arrays.toString(intArray));

        //Multi-dimensional Arrays
        //Syntax:
            //dataType[][] identifier = new dataType[rowLength][colLength]

        String[][] classroom = new String[3][3];
        //First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Prothos";
        classroom[0][2] = "Aramis";
        //Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Jane";
        classroom[1][2] = "Jobert";
        //Third Row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        //Printing multidimensional array
        System.out.println(Arrays.toString(classroom));
        //We use the deepToString() method for printing multidimensional array
        System.out.println(Arrays.deepToString(classroom));

        //ArrayLists
            //Are resizable arrays, where in elements can be added or removed whenever it is needed

            //Syntax:
                //ArrayList<dataType> identifier = new ArrayList<dataType>()

        //Declare an arrayList
        ArrayList<String> students = new ArrayList<>();//for String

        //Adding elements to ArrayList is done by the add() function
        students.add("John");
        students.add("Paul");

        System.out.println(students);


        //Declare an ArrayList with values/elements
        ArrayList<String> student = new ArrayList<> (Arrays.asList("Johns","Pauls"));
        System.out.println(student);

        //Access specific elements in the ArrayList
        //Accessing elements to an ArrayList is done by the get() function
        System.out.println(student.get(1));

        //Updating an element in ArrayList
        //Updating elements to an ArrayList is done by tge set() function
        student.set(1,"George");
        System.out.println(student);

        //Adding an element on a specific index
        //Syntax: ArrayListName.add(index, value/element)
        student.add(1,"Mike");
        System.out.println(student);

        //Removing a specific element in the ArrayList
        //Using remove()
        student.remove(1);
        System.out.println(student);

        //Removing all elements in the ArrayList
        student.clear();
        System.out.println(student);

        //Getting the number of elements in an ArrayList
        System.out.println(student.size());

        //Wrapper Class: Integer
        //Integer - is object version of the int dataType
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1,2));
        System.out.println(numbers);

        //HashMaps
            //Collection of data in "key-value pairs"
            //In Java, "keys" also referred by the "fields"
            //wherein the values are accessed by the "fields"

        //Syntax:
            //HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<dataTypeField, dataTypeValue>;

        //Declaring HashMaps
        HashMap<String, String> jobPosition = new HashMap<>();
        System.out.println(jobPosition);

        //Add elements to HashMap by using the put() function
        jobPosition.put("Student", "Alice");
        jobPosition.put("Developer", "Magic");
        System.out.println(jobPosition);

        //Creating HashMaps with Initialization
        HashMap<String, String> jobPositions = new HashMap<>(){
            {
                put("Student","Alice");
                put("Developer", "Magic");
            }
        };
        System.out.println(jobPositions);

        //Accessing elements in HashMaps
        System.out.println(jobPositions.get("Developer"));

        //Updating an element
        jobPositions.replace("Student","Jacob");
        System.out.println(jobPositions);

        // if the field is already existing it will override the element in the collection
        jobPositions.put("Student", "Jacob P");
        System.out.println(jobPositions);

        //print only fields in the hashmap
        System.out.println(jobPositions.keySet());

        //print only the value in the hashmap
        System.out.println(jobPositions.values());

        //Remove a specific element in the hashmap collection
        jobPositions.remove("Student");
        System.out.println(jobPositions);


        //Removing all Elements
        jobPositions.clear();
        System.out.println(jobPositions);
    }
}
