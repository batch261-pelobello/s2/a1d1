package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Activity2 {
    public static void main(String[] args){

        Integer[] nums= {2,3,5,7,11};
        System.out.println("The first prime number is: "+ nums[0]);

        ArrayList<String> myFriends = new ArrayList<> (Arrays.asList("John","Jane","Chloe","Zhoe"));
        System.out.println("My Friends are: "+ myFriends);

        HashMap<String, Integer> inventory = new HashMap<>(){
            {
                put("toothpaste",15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };
        System.out.println("Our Current Inventory consists of: " +inventory);
    }
}
